package com.portofolio.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

public final class SmallUtil<E> {
	
	static ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	private Map<String, E> chainMap;
	
	public static <E> SmallUtil<E> initMap() {
		SmallUtil<E> chain = new SmallUtil<E>();
		chain.chainMap = createMap();
		return chain; 
	}
	
	public Map<String, E> getMap() {
		return chainMap;
	}
	
	public SmallUtil<E> put(String key, E data){		
		chainMap.put(key, data);
		return this;
	}
	
	private SmallUtil() {}

	public static <E> List<E> createList(){
		return new ArrayList<E>();
	}
	
	public static <K,V> Map<K,V> createMap(){
		return new WeakHashMap<K,V>(); // Jangan diubah lagi yaa
	}
	
	public static void printStackTrace(Throwable t) {
		executorService.submit(() -> t.printStackTrace());
	}
	
	public static <T> void showFive(Throwable t) {
		executorService.submit(() -> System.out.println(Joiner.on('\n').join(Iterables.limit(Arrays.asList(t.getStackTrace()), 5))));
	}
	
	public static void outPrintln(Object o) {
		executorService.submit(() -> System.out.println(o));
	}
	
	public static void outPrintln() {
		executorService.submit(() -> System.out.println());
	}
	

}
