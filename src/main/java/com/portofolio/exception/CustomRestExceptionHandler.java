package com.portofolio.exception;

import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.common.base.Predicates;
import com.google.common.base.Throwables;
import com.google.common.collect.Iterables;
import com.portofolio.exception.ApiError;
import com.portofolio.exception.InfoException;
import com.portofolio.language.MessageByLocaleService;
import com.portofolio.util.SmallUtil;

//
//<version>1.2.3</version>
//
@ControllerAdvice(annotations = RestController.class)
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	MessageByLocaleService messageByLocaleService;
	

	String getErrorMessage(String errorMessage) {
		String errorMessageNew = errorMessage;
		try {
			String errorMessageTmp = messageByLocaleService.getMessage(errorMessage);
			errorMessageNew = errorMessageTmp;
		} catch (Exception exception) {
		}
		return errorMessageNew;
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		List<Map<String, Object>> data = SmallUtil.createList();
		
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			Map<String, Object> rm = SmallUtil.createMap();
			String errorMessage = error.getDefaultMessage();
			errorMessage = getErrorMessage(errorMessage);
			rm.put("error", errorMessage);
			data.add(rm);
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			Map<String, Object> rm = SmallUtil.createMap();
			String errorMessage = error.getDefaultMessage();
			errorMessage = getErrorMessage(errorMessage);
			rm.put("error", errorMessage);
			data.add(rm);
		}

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	@Override
	//@ExceptionHandler({ BindException.class })
	protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);
		logger.info(ex.getClass().getName());
		List<Map<String, Object>> data = SmallUtil.createList();

		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			Map<String, Object> rm = SmallUtil.createMap();
			String errorMessage = error.getDefaultMessage();
			errorMessage = getErrorMessage(errorMessage);
			rm.put("error", errorMessage);
			data.add(rm);
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			Map<String, Object> rm = SmallUtil.createMap();
			String errorMessage = error.getDefaultMessage();
			errorMessage = getErrorMessage(errorMessage);
			rm.put("error", errorMessage);
			data.add(rm);
		}
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	@Override
	//@ExceptionHandler({ TypeMismatchException.class })
	protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);
		logger.info(ex.getClass().getName());
		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();

		final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType();
		rm.put("error", error);
		data.add(rm);
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	@Override
	//@ExceptionHandler({ MissingServletRequestPartException.class })
	protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);
		logger.info(ex.getClass().getName());
		final String error = ex.getRequestPartName() + " part is missing";
		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();
		rm.put("error", error);
		data.add(rm);

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	@Override
	//@ExceptionHandler({ MissingServletRequestParameterException.class })
	protected ResponseEntity<Object> handleMissingServletRequestParameter(final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status,  final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		final String error = ex.getParameterName() + " parameter is missing";
		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();
		rm.put("error", error);
		data.add(rm);

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	// 404

	@Override
	//@ExceptionHandler({ NoHandlerFoundException.class })
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();
		rm.put("error", error);
		data.add(rm);
		final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	// 405

	@Override
	//@ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append(" method is not supported for this request. Supported methods are ");
		// ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();
		rm.put("error", builder);
		data.add(rm);

		final ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	// 415

	@Override
	//@ExceptionHandler({ HttpMediaTypeNotSupportedException.class })
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getContentType());
		builder.append(" media type is not supported. Supported media types are ");
		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();
		rm.put("error", builder);
		data.add(rm);

		final ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}


	// 400
	
	//@ExceptionHandler({ ConstraintViolationException.class })
	protected ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex, final WebRequest request) {
		SmallUtil.printStackTrace(ex);

		List<Map<String, Object>> data = SmallUtil.createList();

		for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			Map<String, Object> rm = SmallUtil.createMap();
			String errorMessage = violation.getMessage();
			errorMessage = getErrorMessage(errorMessage);
			rm.put("error", errorMessage);
			data.add(rm);
		}

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "ERROR", data, "400");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	
	// "500"

	@ExceptionHandler({ Exception.class })
	@ResponseBody
    public Object handleInfoException(final Exception ex, final WebRequest request) {
		

		final StringBuilder builder = new StringBuilder();
    	
    	ApiError apiError = new ApiError();
    	
    	List<Map<String,Object>> data = SmallUtil.createList();
        Map<String,Object> rm = SmallUtil.createMap();
         
        boolean info = false;	
        
        if (Iterables.any(Throwables.getCausalChain(ex), Predicates.instanceOf(InfoException.class))){   		
        	info = true;
    		builder.append(ex.toString());
    		String replac=builder.toString();
    		replac=replac.replace("com.portofolio.exception.InfoException:", "");
            rm.put("error",replac);
            data.add(rm);
    		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "501");
    	
    	}else if (Iterables.any(Throwables.getCausalChain(ex), Predicates.instanceOf(CustomException.class))){   		
        	info = true;
    		builder.append(ex.toString());
    		String replac=builder.toString();
    		replac=replac.replace("com.portofolio.exception.CustomException:", "");
            rm.put("error",replac);
            data.add(rm);
    		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "500");
    	
    	} else if (Iterables.any(Throwables.getCausalChain(ex), Predicates.instanceOf(NotFoundException.class))){   		
        	info = true;
    		builder.append(ex.toString());
    		String replac=builder.toString();
    		replac=replac.replace("com.portofolio.exception.NotFoundException", "");
            rm.put("error",replac);
            data.add(rm);
    		apiError = new ApiError(HttpStatus.NOT_FOUND, "NOT_FOUND", data, "404");
    	
    	} /*else if (Iterables.any(Throwables.getCausalChain(ex), Predicates.instanceOf(InfoTanyaException.class))){	
    		info = true;
    		builder.append(ex.toString());
    		String replac=builder.toString();
    		replac=replac.replace("com.portofolio.exception.InfoTanyaException:", "");
            rm.put("error",replac);
            data.add(rm);
    		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, 502);
    		
    	} */else if (Iterables.any(Throwables.getCausalChain(ex), Predicates.instanceOf(ConstraintViolationException.class))) {
    		
    		return handleConstraintViolation((ConstraintViolationException) ex, request);
    	
//    	} else if (ex instanceof HttpMediaTypeNotSupportedException){
//    		
//    		return handleHttpMediaTypeNotSupported((HttpMediaTypeNotSupportedException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof HttpRequestMethodNotSupportedException){
//    		
//    		return handleHttpRequestMethodNotSupported((HttpRequestMethodNotSupportedException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof NoHandlerFoundException){
//    		
//    		return handleNoHandlerFoundException((NoHandlerFoundException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof MissingServletRequestParameterException){
//    		
//    		return handleMissingServletRequestParameter((MissingServletRequestParameterException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof MissingServletRequestPartException){
//    		
//    		return handleMissingServletRequestPart((MissingServletRequestPartException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof TypeMismatchException){
//    		
//    		return handleTypeMismatch((TypeMismatchException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof BindException){
//    		
//    		return handleBindException((BindException) ex, headers, status, request);
//    	
//    	} else if (ex instanceof MethodArgumentNotValidException){
//    		
//    		return handleMethodArgumentNotValid((MethodArgumentNotValidException) ex, headers, status, request);
//    	    	
    	} else {	
    		builder.append(ex.toString());
    	    rm.put("error",builder);
    	    data.add(rm);
    		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "500");    		
    	}
    	
    	SmallUtil.outPrintln(ex + ":" +ex.getClass().getName());
    	SmallUtil.outPrintln(ex + ":" + ex.getCause());
		//logger.warn("CustomRestExceptionHandler baris 305 - ex.getClass().getName() : " + ex.getClass().getName());
		//logger.warn("CustomRestExceptionHandler baris 306 - ex.getCause() : " + ex.getCause());
   	   
    	if(info) {
    		SmallUtil.showFive(ex);
    	} else {
    		SmallUtil.printStackTrace(ex);    		
    	}
    	
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}



//@ExceptionHandler({ Exception.class })
//@ResponseBody
//protected ResponseEntity<Object> handleAnyException(Exception ex) {
//	InternalUtil.printStackTrace(ex);
//	
//	final StringBuilder builder = new StringBuilder();
//	
//	ApiError apiError = new ApiError();
//	
//	List<Map<String,Object>> data = InternalUtil.createList();
//    Map<String,Object> rm = InternalUtil.createMap();
//	
//	builder.append(ex.toString());
//    rm.put("error",builder);
//    data.add(rm);
//	apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "500");
//			
//	return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
//}