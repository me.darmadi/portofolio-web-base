package com.portofolio.base;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.portofolio.util.SmallUtil;

/**
 * Base Controller Class for handling messaga resource for internationalization
 * & locale
 * 
 * @author Syamsu
 */
public abstract class RestfulJsonController {

	<T> Map<String,Object> contructResponse(T t, String pesan, HttpStatus statusCode){
		return SmallUtil.initMap()
				.put("data", t)
				.put("statusCode", statusCode.value())
				.put("code", statusCode.value())
				.put("status", "success")
				.put("message", pesan)
				.getMap();		
	}
	
	protected <T> ResponseEntity<Map<String,Object>> sendRespond(T t, String pesan){
		return sendRespond(t, pesan, HttpStatus.OK);
	}
	
	protected <T> ResponseEntity<Map<String,Object>> sendRespond(T t, String pesan, HttpStatus statusCode){
		return ResponseEntity.status(statusCode).body(contructResponse(t, pesan, statusCode));
	}
	
	protected <T> ResponseEntity<Map<String,Object>> getRespond(T t){
		return getRespond(t, "Data berhasil ditampilkan.");
	}
	
	protected <T> ResponseEntity<Map<String,Object>> getRespond(T t, String pesan){
		return ResponseEntity.ok().body(contructResponse(t, pesan, HttpStatus.OK));
	}
	
	protected <T> ResponseEntity<Map<String,Object>> postRespond(T t){
		return postRespond(t, "Data berhasil disimpan.");
	}
	
	protected <T> ResponseEntity<Map<String,Object>> postRespond(T t, String pesan){
		return ResponseEntity.status(HttpStatus.CREATED).body(contructResponse(t, pesan, HttpStatus.CREATED));
	}
}
