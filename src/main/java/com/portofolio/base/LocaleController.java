package com.portofolio.base;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.portofolio.language.MessageByLocaleService;
import com.portofolio.util.SmallUtil;

/**
 * Base Controller Class for handling messaga resource for internationalization
 * & locale
 * 
 * @author Adik
 * @modified Syamsu
 */
public abstract class LocaleController<V> {

	// Using WeakHashMap (Syamsu)
	protected Map<String, Object> mapHeaderMessage = SmallUtil.createMap(); // shortcut (Syamsu)
	
	public static final String  stringAsIntegerMaxValue=Integer.MAX_VALUE + "";

	@Autowired
	private MessageByLocaleService messageByLocaleService;

	protected void addHeaderMessage(String key, String message) {
		this.mapHeaderMessage.put(key, message);
	}

	protected String getMessage(String message) {
		return messageByLocaleService.getMessage(message);
	}
	
	protected Map<String, Object> wrapper(Map<String, Object> data){ // shortcut (Syamsu)
		Map<String, Object> result = SmallUtil.createMap();
		result.put("data", data);
		
		return result;
	}
	
	protected Map<String, Object> wrapper(List<Map<String, Object>> data){ // shortcut (Syamsu)
		Map<String, Object> result = SmallUtil.createMap();
		result.put("data", data);
		
		return result;
	}

}
