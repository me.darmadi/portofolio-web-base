package com.portofolio.swagger.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SwaggerUIController {

	@GetMapping("/")
    public String index(HttpServletResponse res) {
		//res.setStatus(HttpStatus.TEMPORARY_REDIRECT.value());
        return "redirect:/swagger-ui.html";
    }
}
